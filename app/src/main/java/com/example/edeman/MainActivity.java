package com.example.edeman;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


public class MainActivity extends AppCompatActivity {

    private EditText txt_nombre;
    private TextView txt_area_ingre,txt_area_nutri,txt_title_receta;
    private Button bt_buscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txt_area_ingre = (TextView) findViewById(R.id.txt_area_ingre);
        this.txt_area_nutri = (TextView) findViewById(R.id.txt_area_nutri);
        this.txt_nombre = (EditText) findViewById(R.id.txt_nombre);
        this.bt_buscar = (Button) findViewById(R.id.bt_buscar);
        this.txt_title_receta = (TextView)findViewById(R.id.txt_title_receta) ;

        bt_buscar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                String nombre = txt_nombre.getText().toString().trim();
                String appidx = "51086025";
                String appkey = "27a489c61d8e1d1f1c33bc5a7aa01a41";



                String url = "https://test-es.edamam.com/search?q="+nombre+"&app_id="+appidx+"&app_key="+appkey;
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);


                                    JSONArray hitsJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject primer = hitsJSON.getJSONObject(0);
                                    JSONObject recipesJSON  = primer.getJSONObject("recipe");
                                    String titulo = recipesJSON.getString("label");
                                    txt_title_receta.setText(titulo);
                                    JSONArray ingredientesARRAY = recipesJSON.getJSONArray("ingredientLines");
                                    String ingredientes = "" ;

                                    for(int i = 0; i<ingredientesARRAY.length();i++){

                                        ingredientes = ingredientes+ ingredientesARRAY.getString(i)+ "\n";


                                    }
                                    txt_area_ingre.setText(ingredientes);

                                    JSONObject nutJSON = recipesJSON.getJSONObject("totalNutrients");
                                    JSONObject enerJSON =nutJSON.getJSONObject("ENERC_KCAL");
                                    String enerlabel = enerJSON.getString("label");
                                    String enerq = enerJSON.getString("quantity");
                                    String eneru = enerJSON.getString("unit");

                                    JSONObject fatJSON =nutJSON.getJSONObject("FAT");
                                    String fatlabel =fatJSON.getString("label");
                                    String fatq = fatJSON.getString("quantity");
                                    String fatu = fatJSON.getString("unit");

                                    JSONObject fasatJSON =nutJSON.getJSONObject("FASAT");
                                    String fasatlabel = fasatJSON.getString("label");
                                    String fasatq = fasatJSON.getString("quantity");
                                    String fasatu = fasatJSON.getString("unit");


                                    String todo = enerlabel + " - " + enerq + " - " + eneru + "\n" +
                                            fatlabel + " - " + fatq + " - " + fatu + "\n" +
                                            fasatlabel + " - " + fasatq + " - " + fasatu + "\n" ;

                                    txt_area_nutri.setText(todo);

                                    txt_nombre.setText("");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);


            }
        });
    }



}
